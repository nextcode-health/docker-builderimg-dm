## Dockerfile with Python2 and git

This repository contains **Dockerfile** for the container used for building [data_man](https://bitbucket.org/nextcode-health/data_man/) project at NextCODE. It uses [Docker](https://www.docker.com/)'s [automated build](https://registry.hub.docker.com/u/nextcode/builderimg-dm/) published to the public [Docker Hub Registry](https://registry.hub.docker.com/).


### Base Docker Image

* [library/centos](https://hub.docker.com/_/centos/)


### Docker Tags

`nextcode/builderimg-dm` provides the following tagged image:

* `latest` (default): built from master branch in Bitbucket


### Installation

1. Install [Docker](https://www.docker.com/).

2. Download [automated build](https://registry.hub.docker.com/u/nextcode/builderimg-dm/) from private [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull nextcode/builderimg-dm`
